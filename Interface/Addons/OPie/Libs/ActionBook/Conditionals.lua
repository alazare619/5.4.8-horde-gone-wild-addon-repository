local _, T = ...
local KR, EV = assert(T.ActionBook:compatible("Kindred",1,3), "A compatible version of Kindred is required"), T.Evie
local _, playerClassLocal, playerClass = T.Toboe and T.Toboe(), UnitClass("player")

local safequote do
	local r = {u="\\117", ["{"]="\\123", ["}"]="\\125"}
	function safequote(s)
		return (("%q"):format(s):gsub("[{}u]", r))
	end
end

local RegisterStateConditional do
	local f = CreateFrame("FRAME", nil, nil, "SecureHandlerAttributeTemplate")
	f:SetFrameRef("KR", KR:seclib())
	f:Execute('KR, cndName, curValue = self:GetFrameRef("KR"), newtable(), newtable()')
	f:SetAttribute("_onattributechanged", [[--
		local state = name:match("^state%-(.+)")
		if state and value ~= "_" then
			KR:RunAttribute("UpdateStateConditional", cndName[state], value, curValue[state] or "")
			curValue[state] = value
		end
	]])
	function RegisterStateConditional(name, forName, conditional, isExtended)
		f:SetAttribute(forName, "_")
		f:Execute(([[-- KR-InternalStateConditionalRemap
			local name, forName, conditional, isExt = %q, %q, %s, %s
			cndName[name] = forName
			if isExt then
				KR:SetAttribute("frameref-RegisterStateDriver-frame", self)
				KR:RunAttribute("RegisterStateDriver", name, conditional)
			else
				RegisterStateDriver(self, name, conditional)
			end
		]]):format(name, forName, safequote(conditional), isExtended and 1 or "nil"))
	end
end

do -- zone:Zone/Sub Zone
	local function onZoneUpdate()
		local cz
		for i=1,4 do
			local z = (i == 1 and GetRealZoneText or i == 2 and GetSubZoneText or i == 3 and GetZoneText or GetMinimapZoneText)()
			if z and z ~= "" then
				cz = (cz and (cz .. "/") or "") .. z
			end
		end
		KR:SetStateConditionalValue("zone", cz or false)
	end
	onZoneUpdate()
	EV.RegisterEvent("ZONE_CHANGED", onZoneUpdate)
	EV.RegisterEvent("ZONE_CHANGED_INDOORS", onZoneUpdate)
	EV.RegisterEvent("PLAYER_ENTERING_WORLD", onZoneUpdate)
end
do -- me:Player Name/Class
	KR:SetStateConditionalValue("me", UnitName("player") .. "/" .. playerClassLocal .. "/" .. playerClass)
end
do -- known:spell id
	KR:SetSecureExecConditional("known", [=[-- KR_KnownSpell
		local ids = ...
		for sid in (type(ids) == "string" and ids or ""):gmatch("[^/]+") do
			local id = tonumber(sid:match("^%s*(%d+)%s*$"))
			if id and (FindSpellBookSlotBySpellID(id, true) or FindSpellBookSlotBySpellID(id, false)) then
				return true
			end
		end
		return false
	]=])
	local f = CreateFrame("FRAME")
	f:SetScript("OnUpdate", function(self)
		self:Hide()
		KR:PokeConditional("known")
	end)
	f:SetScript("OnEvent", f.Show)
	f:RegisterEvent("LEARNED_SPELL_IN_TAB")
	f:RegisterEvent("PLAYER_SPECIALIZATION_CHANGED")
	f:RegisterEvent("PLAYER_ENTERING_WORLD")
end
do -- spec:Spec Name
	RegisterStateConditional("spec1", "spec", "[spec:1] 1; [spec:2] 2", false)
	EV.RegisterEvent("PLAYER_LOGIN", function()
		local cnd, ns, sp, sl, sbest = nil, GetNumSpecializations(), {}, {}, {}
		local function pop(id, sid, lvl, ...)
			if sid then
				sp[sid], sl[sid] = sp[sid] == nil and id, lvl
				return pop(id, ...)
			end
		end
		for i=1,ns do
			pop(i, GetSpecializationSpells(i))
		end
		for sid, i in pairs(sp) do
			if i and (sbest[i] == nil or sl[sbest[i]] > sl[sid]) then
				sbest[i] = sid
			end
		end
		for i=1,ns do
			local id, name = GetSpecializationInfo(i)
			cnd = (cnd and (cnd .. "; ") or "") .. "[known:" .. sbest[i] .. "] " .. id .. "/" .. name
		end
		RegisterStateConditional("spec2", "spec", cnd .. "; -1/unspec", true)
		return "remove"
	end)
end
do -- form:token
	if playerClass == "DRUID" then
		KR:SetAliasConditional("stance", "form")
		local map, curCnd, pending = {
			[GetSpellInfo(40120)]="/flight",
			[GetSpellInfo(33943)]="/flight",
			[GetSpellInfo(1066)]="/aquatic",
			[GetSpellInfo(783)]="/travel",
			[GetSpellInfo(24858)]="/moon",
			[GetSpellInfo(768)]="/cat",
			[GetSpellInfo(171745) or 1]="/cat",
			[GetSpellInfo(5487)]="/bear",
		}
		local function sync()
			local s = ""
			for i=1,10 do
				local _, name = GetShapeshiftFormInfo(i)
				s = ("%s[form:%d] %d%s;"):format(s, i,i, map[name] or "")
			end
			if curCnd ~= s then
				RegisterStateConditional("form", "form", s, false)
			end
			pending = nil
			return "remove"
		end
		EV.RegisterEvent("PLAYER_LOGIN", sync)
		EV.RegisterEvent("UPDATE_SHAPESHIFT_FORMS", function()
			if not InCombatLockdown() then
				sync()
			elseif not pending then
				pending = 1
				EV.RegisterEvent("PLAYER_REGEN_ENABLED", sync)
			end
		end)
	end
end
do -- talent:tier.num/name
	local cur, levels = false, CLASS_TALENT_LEVELS[playerClass] or CLASS_TALENT_LEVELS.DEFAULT
	local function updateTalents()
		local s
		for i=1,GetNumTalents() do
			local name, _, tier, column, selected = GetTalentInfo(i)
			if selected then
				s = (s and s .. "/" or "") .. tier .. "." .. column .. "/" .. levels[tier] .. "." .. column .. "/" .. name
			end
		end
		if s ~= cur then
			cur = s
			KR:SetStateConditionalValue("talent", cur or "")
		end
	end
	EV.RegisterEvent("PLAYER_TALENT_UPDATE", updateTalents)
	updateTalents()
end
do -- instance:arena/bg/ratedbg/raid/instance/scenario
	local mapTypes = {party="dungeon", pvp="battleground/bg", ratedbg="ratedbg/rgb", none="world"}
	for n in ("1158 1331 1159 1152 1330 1153"):gmatch("%d+") do mapTypes[tonumber(n)] = "world/garrison" end
	EV.RegisterEvent("PLAYER_ENTERING_WORLD", function()
		local _, itype, did, _, _, _, _, imid = GetInstanceInfo()
		if imid and mapTypes[imid] then
			itype = mapTypes[imid]
		elseif itype == "pvp" and IsRatedBattleground() then
			itype = "ratedbg"
		elseif itype == "none" and IsInActiveWorldPVP() then
			itype = "worldpvp"
		elseif itype == "raid" then
			local _, _, did = GetInstanceInfo()
			if did == 7 then
				itype = "lfr"
			end
		end
		KR:SetStateConditionalValue("in", mapTypes[itype] or itype)
	end)
	KR:SetAliasConditional("instance", "in")
	KR:SetStateConditionalValue("in", "daze")
end