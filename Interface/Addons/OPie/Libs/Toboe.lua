if select(4,GetBuildInfo()) < 6e4 then return end
local E, _, T = {}, ...
local tip = CreateFrame("GameTooltip")
tip:AddFontStrings(tip:CreateFontString(), tip:CreateFontString())

do -- GetSpellInfo
	local function filter(...)
		local a, b, c = ...
		if not (a == "" and b == "" and c == "") then
			return ...
		end
	end
	function E.GetSpellInfo(...)
		return filter(GetSpellInfo(...))
	end
end
do -- GetNumCompanions
	function E.GetNumCompanions(...)
		if ... == "MOUNT" then
			local c = 0
			for i=1,C_MountJournal.GetNumMounts() do
				local _1, _2, _3, _4, _5, _6, _7, _8, _9, hide, have = C_MountJournal.GetMountInfo(i)
				if have and not hide then
					c = c + 1
				end
			end
			return c
		else
			return GetNumCompanions(...)
		end
	end
end
do -- GetCompanionInfo
	local mtm, lastIndex, lastJournalIndex = {[230]=29, [248]=11}
	function E.GetCompanionInfo(...)
		local ctype, index = ...
		if not (ctype == "MOUNT" and type(index) == "number") then
			return GetCompanionInfo(...)
		end
		local si = 1
		if lastIndex == (index-1) and lastJournalIndex then
			si, lastIndex, index = lastJournalIndex + 1, index, index - lastIndex
		else
			lastIndex = index
		end
		
		for i=si,C_MountJournal.GetNumMounts() do
			local name, sid, icon, isSummoned, isUsable, _6, isFavorite, _8, _9, hide, have = C_MountJournal.GetMountInfo(i)
			if have and not hide then
				if index == 1 then
					local _, _, _, _, mtype = C_MountJournal.GetMountInfoExtra(i)
					lastJournalIndex = i
					return -sid, name, sid, icon, isSummoned and 1 or nil, mtm[mtype] or 0, isUsable and (isFavorite and 2 or 1) or 0
				end
				index = index - 1
			end
		end
	end
end
do -- Is*InRange
	local m = {[0]=0, 1, [true]=1, [false]=0}
	local function normalized(f)
		return function(...)
			return m[f(...)]
		end
	end
	for k in ("IsSpellInRange IsItemInRange IsActionInRange"):gmatch("%S+") do
		E[k] = normalized(_G[k])
	end
end

function E.GetNumTalents()
	return 21
end
function E.GetTalentInfo(id, isInspect, talentGroup, inspectedUnit, classId)
	local col = ((id-1) % 3)+1
	local row = 1 + (id - col)/3
	local sid, name, tex, selected, available = GetTalentInfo(row, col, talentGroup or GetActiveSpecGroup() or 1, isInspect, inspectedUnit)
	return name, tex, row, col, selected, available
end
function E.GetTalentSpellForSpecialization(id, spec)
	tip:SetOwner(UIParent, "ANCHOR_NONE")
	local col = ((id-1) % 3)+1
	tip:SetTalent((GetTalentInfoBySpecialization(spec or GetSpecialization() or 1, 1 + (id - col)/3, col)))
	return tip:GetSpell()
end

E.MAX_ACCOUNT_MACROS = 120
do -- Macro icons and file data
	local tex = WorldFrame:CreateTexture()
	local function wrap(f)
		return function(t)
			local fi, t = (t and #t or 0), f(t)
			for i=fi+1,#t do
				if type(t[i]) == "number" then
					t[i] = (tex:SetToFileData(t[i]) and nil) or tex:GetTexture():match("[^\\]+$")
				end
			end
			return t
		end
	end
	E.GetMacroIcons, E.GetMacroItemIcons = wrap(GetMacroIcons), wrap(GetMacroItemIcons)
end

function T.Toboe()
	local G, N = getfenv(2), {}
	for k,v in pairs(E) do N[k] = v end
	N._G = N
	setmetatable(N, {__index=G, __newindex=function(t,k,v) G[k] = v end})
	setfenv(2, N)
end