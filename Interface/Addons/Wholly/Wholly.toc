## Interface: 60000
## Title: Wholly
## Author: Scott M Harrison
## Notes: Shows quest database and map pins
## Version: 046
## Dependencies: Grail
## OptionalDeps: LibStub, LibDataBroker, TomTom, Gatherer, OmegaMap, Carbonite
## SavedVariablesPerCharacter: WhollyDatabase

Wholly.lua
Wholly.xml
