# Horde Gone Wild Addon & Variable Data for 5.4.8 TauriWOW [EN]-Evermoon

## Installation!
 - In order to use the Addons and Data download a zipped version of the repository from [here.](https://gitlab.com/alazare619/5.4.8-horde-gone-wild-addon-repository/-/archive/master/5.4.8-horde-gone-wild-addon-repository-master.zip)
  - Extract the zip into the root of your wow folder
  - Under the saved variables folder rename it to what your username is when you login to TauriWOW (Wow Launcher Username)
  - Addons will just show up normally
  - For Profiles/Saved Variables you will need to go to the addons configuration and choose the name of the profile it will be in the format of "2020xxx"
## New Features!
  - 09/16/20 Added missing AckisRecipe Lists so you can see where every missing profession skill comes from
  - 09/15/20 Added Saved Variables for skada,elvui,gatherer,bigwigs and parrot for uniform ui and restructured repo as such
  - 09/07/20 Created Repo for addons


## ContactUS
 - Join Our Discord [click here](https://discord.gg/aSH2Xtr)
