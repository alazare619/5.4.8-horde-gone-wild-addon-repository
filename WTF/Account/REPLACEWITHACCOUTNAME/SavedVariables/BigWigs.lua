BigWigs3DB = {
	["namespaces"] = {
		["BigWigs_Bosses_The Fallen Protectors"] = {
			["profiles"] = {
				["2020 Tank"] = {
					["Vengeful Strikes"] = 1539,
					["Gouge"] = 1539,
				},
			},
		},
		["BigWigs_Bosses_Primordius"] = {
			["profiles"] = {
				["Feral"] = {
					[-6969] = 1539,
				},
				["Default"] = {
					[-6969] = 1539,
				},
			},
		},
		["BigWigs_Bosses_Siegecrafter Blackfuse"] = {
			["profiles"] = {
				["2020 Tank"] = {
					["Electrostatic Charge"] = 1539,
				},
				["2020 Default"] = {
					["Electrostatic Charge"] = 1539,
				},
			},
		},
		["BigWigs_Plugins_Alt Power"] = {
			["profiles"] = {
				["Default"] = {
					["posx"] = 362.6661763164611,
					["fontSize"] = 15,
					["fontOutline"] = "OUTLINE",
					["posy"] = 128.7110491236035,
					["font"] = "Expressway",
				},
				["2020 Heals"] = {
					["posx"] = 362.6661763164611,
					["fontSize"] = 15,
					["fontOutline"] = "OUTLINE",
					["posy"] = 128.7110491236035,
					["font"] = "Expressway",
				},
				["2020 Default"] = {
					["posx"] = 362.6661763164611,
					["fontSize"] = 15,
					["fontOutline"] = "OUTLINE",
					["font"] = "Expressway",
					["posy"] = 128.7110491236035,
				},
				["2020 Range"] = {
					["posx"] = 362.6661763164611,
					["fontSize"] = 15,
					["fontOutline"] = "OUTLINE",
					["posy"] = 128.7110491236035,
					["font"] = "Expressway",
				},
				["2020 Melee"] = {
					["posx"] = 362.6661763164611,
					["fontSize"] = 15,
					["fontOutline"] = "OUTLINE",
					["posy"] = 128.7110491236035,
					["font"] = "Expressway",
				},
				["2020 Tank"] = {
					["posx"] = 362.6661763164611,
					["fontSize"] = 15,
					["fontOutline"] = "OUTLINE",
					["font"] = "Expressway",
					["posy"] = 128.7110491236035,
				},
			},
		},
		["LibDualSpec-1.0"] = {
			["char"] = {
				["Malvnance - [EN] Evermoon"] = {
					["profile"] = "2020 Tank",
					["specGroup"] = 1,
					["enabled"] = true,
				},
			},
		},
		["BigWigs_Bosses_Malkorok"] = {
			["profiles"] = {
				["Feral"] = {
					["Breath of Y'Shaarj"] = 643,
				},
				["2020 Range"] = {
					["Breath of Y'Shaarj"] = 643,
				},
			},
		},
		["BigWigs_Plugins_Sounds"] = {
		},
		["BigWigs_Bosses_Iron Juggernaut"] = {
			["profiles"] = {
				["2020 Default"] = {
					["Ignite Armor"] = 4611,
				},
			},
		},
		["BigWigs_Plugins_Statistics"] = {
		},
		["BigWigs_Bosses_Ra-den Assist"] = {
			["profiles"] = {
				["Feral"] = {
					["posx"] = 219.3776528815397,
					["firstredkillcount"] = 4,
					["disableWarn"] = true,
					["posy"] = 539.3778776440377,
					["width"] = 155,
					["lock"] = true,
					["height"] = 233,
					["group2icon"] = 3,
					["group1icon"] = 1,
					["restredkillcount"] = 2,
				},
				["2020 Default"] = {
					["posx"] = 188.0886764294519,
					["firstredkillcount"] = 4,
					["group2icon"] = 3,
					["group1icon"] = 1,
					["height"] = 233,
					["restredkillcount"] = 2,
					["posy"] = 333.8667398552116,
					["width"] = 155,
				},
			},
		},
		["BigWigs_Bosses_Dark Animus"] = {
			["profiles"] = {
				["Feral"] = {
					["Interrupting Jolt"] = 643,
					["Anima Ring"] = 515,
				},
				["Default"] = {
					["Anima Ring"] = 515,
					["Interrupting Jolt"] = 643,
				},
				["2020 Default"] = {
					["Anima Ring"] = 515,
				},
			},
		},
		["BigWigs_Bosses_Durumu the Forgotten"] = {
			["profiles"] = {
				["Feral"] = {
					[-6905] = 675,
					["Lingering Gaze"] = 482,
					["initial_life_drain"] = 643,
				},
				["Default"] = {
					[-6905] = 675,
					["Lingering Gaze"] = 482,
					["initial_life_drain"] = 643,
				},
			},
		},
		["BigWigs_Bosses_Garrosh Hellscream"] = {
			["profiles"] = {
				["2020 Range"] = {
					["Desecrate"] = 675,
				},
				["2020 Default"] = {
					["ironstar"] = 515,
				},
			},
		},
		["BigWigs_Plugins_Colors"] = {
			["profiles"] = {
				["Default"] = {
					["barBackground"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0, -- [2]
								0, -- [3]
								0.4600000381469727, -- [4]
							},
						},
					},
					["barColor"] = {
						["BigWigs_Bosses_Kor'kron Dark Shaman"] = {
							["Froststorm Strike"] = {
								0, -- [1]
								1, -- [2]
								0.9372549019607843, -- [3]
							},
							["Ashen Wall"] = {
								1, -- [1]
								0, -- [2]
								0.6745098039215687, -- [3]
							},
						},
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Thok the Bloodthirsty"] = {
							[-7963] = {
								0.9294117647058824, -- [1]
								1, -- [2]
								0.984313725490196, -- [3]
							},
						},
						["BigWigs_Bosses_Paragons of the Klaxxi"] = {
							["injection_tank"] = {
								0, -- [1]
								0.8666666666666667, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								1, -- [1]
								0.1098039215686275, -- [2]
								0, -- [3]
							},
							["Seismic Slam"] = {
								0, -- [1]
								1, -- [2]
								0.01176470588235294, -- [3]
							},
						},
					},
					["barEmphasized"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.1568627450980392, -- [1]
								0.3490196078431372, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								nil, -- [1]
								0.06666666666666667, -- [2]
							},
						},
					},
				},
				["2020 Heals"] = {
					["barEmphasized"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.1568627450980392, -- [1]
								0.3490196078431372, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								nil, -- [1]
								0.06666666666666667, -- [2]
							},
						},
					},
					["barColor"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								1, -- [1]
								0.1098039215686275, -- [2]
								0, -- [3]
							},
						},
					},
					["barBackground"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0, -- [2]
								0, -- [3]
								0.4600000381469727, -- [4]
							},
						},
					},
				},
				["2020 Default"] = {
					["barColor"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								1, -- [1]
								0.1098039215686275, -- [2]
								0, -- [3]
							},
						},
					},
					["barEmphasized"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.1568627450980392, -- [1]
								0.3490196078431372, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								nil, -- [1]
								0.06666666666666667, -- [2]
							},
						},
					},
					["barBackground"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0, -- [2]
								0, -- [3]
								0.4600000381469727, -- [4]
							},
						},
					},
				},
				["2020 Range"] = {
					["barColor"] = {
						["BigWigs_Bosses_Sha of Pride"] = {
							[-8262] = {
								0.9686274509803922, -- [1]
								1, -- [2]
								0, -- [3]
							},
						},
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								1, -- [1]
								0, -- [2]
								0.02745098039215686, -- [3]
							},
						},
					},
					["barEmphasized"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.1568627450980392, -- [1]
								0.3490196078431372, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								nil, -- [1]
								0.06666666666666667, -- [2]
							},
						},
					},
					["barBackground"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0, -- [2]
								0, -- [3]
								0.4600000381469727, -- [4]
							},
						},
					},
				},
				["2020 Melee"] = {
					["barEmphasized"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.1568627450980392, -- [1]
								0.3490196078431372, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								nil, -- [1]
								0.06666666666666667, -- [2]
							},
						},
					},
					["barColor"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								1, -- [1]
								0.1098039215686275, -- [2]
								0, -- [3]
							},
						},
					},
					["barBackground"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0, -- [2]
								0, -- [3]
								0.4600000381469727, -- [4]
							},
						},
					},
				},
				["2020 Tank"] = {
					["barBackground"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0, -- [1]
								0, -- [2]
								0, -- [3]
								0.4600000381469727, -- [4]
							},
						},
					},
					["barColor"] = {
						["BigWigs_Bosses_Paragons of the Klaxxi"] = {
							["injection_tank"] = {
								0, -- [1]
								0.8666666666666667, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								1, -- [1]
								1, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Thok the Bloodthirsty"] = {
							[-7963] = {
								0.9294117647058824, -- [1]
								1, -- [2]
								0.984313725490196, -- [3]
							},
						},
						["BigWigs_Bosses_Kor'kron Dark Shaman"] = {
							["Froststorm Strike"] = {
								0, -- [1]
								1, -- [2]
								0.9372549019607843, -- [3]
							},
							["Ashen Wall"] = {
								1, -- [1]
								0, -- [2]
								0.6745098039215687, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								1, -- [1]
								0.1098039215686275, -- [2]
								0, -- [3]
							},
							["Seismic Slam"] = {
								0, -- [1]
								1, -- [2]
								0.01176470588235294, -- [3]
							},
						},
					},
					["barEmphasized"] = {
						["BigWigs_Plugins_Colors"] = {
							["default"] = {
								0.1568627450980392, -- [1]
								0.3490196078431372, -- [2]
								1, -- [3]
							},
						},
						["BigWigs_Bosses_Malkorok"] = {
							["Breath of Y'Shaarj"] = {
								nil, -- [1]
								0.06666666666666667, -- [2]
							},
						},
					},
				},
			},
		},
		["BigWigs_Plugins_Raid Icons"] = {
		},
		["BigWigs_Plugins_Bars"] = {
			["profiles"] = {
				["Default"] = {
					["outline"] = "OUTLINE",
					["fontSize"] = 12,
					["BigWigsAnchor_width"] = 270.0001831054688,
					["BigWigsAnchor_y"] = 149.3337492757473,
					["BigWigsAnchor_x"] = 866.1334411117787,
					["emphasizeTime"] = 15,
					["barStyle"] = "MonoUI",
					["growup"] = false,
					["monochrome"] = false,
					["BigWigsEmphasizeAnchor_x"] = 1181.866521673728,
					["emphasize"] = false,
					["font"] = "Expressway",
					["fill"] = false,
					["scale"] = 1.2,
					["BigWigsEmphasizeAnchor_width"] = 258.0002136230469,
					["BigWigsEmphasizeAnchor_y"] = 233.955669727593,
					["texture"] = "ElvUI Norm",
					["emphasizeMove"] = false,
				},
				["2020 Heals"] = {
					["outline"] = "OUTLINE",
					["emphasize"] = false,
					["scale"] = 1.2,
					["BigWigsAnchor_y"] = 160.0003510965325,
					["BigWigsAnchor_x"] = 809.2442036231296,
					["texture"] = "ElvUI Norm",
					["barStyle"] = "MonoUI",
					["growup"] = false,
					["monochrome"] = false,
					["BigWigsEmphasizeAnchor_x"] = 1181.866521673728,
					["BigWigsEmphasizeAnchor_y"] = 233.955669727593,
					["font"] = "Expressway",
					["BigWigsAnchor_width"] = 270.0001831054688,
					["fontSize"] = 12,
					["emphasizeTime"] = 15,
					["BigWigsEmphasizeAnchor_width"] = 258.0002136230469,
					["fill"] = false,
					["emphasizeMove"] = false,
				},
				["2020 Default"] = {
					["BigWigsEmphasizeAnchor_y"] = 233.955669727593,
					["fontSize"] = 12,
					["BigWigsAnchor_width"] = 270.0001831054688,
					["BigWigsAnchor_y"] = 160.0003510965325,
					["BigWigsAnchor_x"] = 809.2442036231296,
					["emphasizeTime"] = 15,
					["barStyle"] = "MonoUI",
					["growup"] = false,
					["monochrome"] = false,
					["BigWigsEmphasizeAnchor_x"] = 1181.866521673728,
					["outline"] = "OUTLINE",
					["font"] = "Expressway",
					["BigWigsEmphasizeAnchor_width"] = 258.0002136230469,
					["texture"] = "ElvUI Norm",
					["fill"] = false,
					["scale"] = 1.2,
					["emphasize"] = false,
					["emphasizeMove"] = false,
				},
				["2020 Range"] = {
					["outline"] = "OUTLINE",
					["emphasize"] = false,
					["scale"] = 1.2,
					["BigWigsAnchor_y"] = 160.0003510965325,
					["BigWigsAnchor_x"] = 809.2442036231296,
					["texture"] = "ElvUI Norm",
					["barStyle"] = "MonoUI",
					["growup"] = false,
					["monochrome"] = false,
					["BigWigsEmphasizeAnchor_x"] = 1181.866521673728,
					["BigWigsEmphasizeAnchor_width"] = 258.0002136230469,
					["font"] = "Expressway",
					["fill"] = false,
					["BigWigsEmphasizeAnchor_y"] = 233.955669727593,
					["BigWigsAnchor_width"] = 270.0001831054688,
					["fontSize"] = 12,
					["emphasizeTime"] = 15,
					["emphasizeMove"] = false,
				},
				["2020 Melee"] = {
					["outline"] = "OUTLINE",
					["emphasize"] = false,
					["scale"] = 1.2,
					["BigWigsAnchor_y"] = 160.0003510965325,
					["BigWigsAnchor_x"] = 809.2442036231296,
					["texture"] = "ElvUI Norm",
					["barStyle"] = "MonoUI",
					["growup"] = false,
					["monochrome"] = false,
					["BigWigsEmphasizeAnchor_x"] = 1181.866521673728,
					["BigWigsEmphasizeAnchor_y"] = 233.955669727593,
					["font"] = "Expressway",
					["BigWigsAnchor_width"] = 270.0001831054688,
					["fontSize"] = 12,
					["emphasizeTime"] = 15,
					["BigWigsEmphasizeAnchor_width"] = 258.0002136230469,
					["fill"] = false,
					["emphasizeMove"] = false,
				},
				["2020 Tank"] = {
					["outline"] = "OUTLINE",
					["emphasize"] = false,
					["scale"] = 1.2,
					["BigWigsAnchor_y"] = 160.0003510965325,
					["BigWigsAnchor_x"] = 809.2442036231296,
					["fill"] = false,
					["barStyle"] = "MonoUI",
					["growup"] = false,
					["monochrome"] = false,
					["BigWigsEmphasizeAnchor_x"] = 1181.866521673728,
					["BigWigsEmphasizeAnchor_y"] = 233.955669727593,
					["font"] = "Expressway",
					["emphasizeTime"] = 15,
					["texture"] = "ElvUI Norm",
					["BigWigsEmphasizeAnchor_width"] = 258.0002136230469,
					["BigWigsAnchor_width"] = 270.0001831054688,
					["fontSize"] = 12,
					["emphasizeMove"] = false,
				},
			},
		},
		["BigWigs_Bosses_Paragons of the Klaxxi"] = {
			["profiles"] = {
				["2020 Default"] = {
					["Whirling"] = 515,
					["injection_tank"] = 1539,
				},
				["2020 Tank"] = {
					["injection_tank"] = 1539,
					["Gouge"] = 4611,
					["Shield Bash"] = 4611,
				},
			},
		},
		["BigWigs_Plugins_Super Emphasize"] = {
			["profiles"] = {
				["Default"] = {
					["font"] = "Friz Quadrata TT",
				},
				["2020 Heals"] = {
					["font"] = "Friz Quadrata TT",
				},
				["2020 Default"] = {
					["font"] = "Friz Quadrata TT",
				},
				["2020 Range"] = {
					["font"] = "Friz Quadrata TT",
				},
				["2020 Melee"] = {
					["font"] = "Friz Quadrata TT",
				},
				["2020 Tank"] = {
					["font"] = "Friz Quadrata TT",
				},
			},
		},
		["BigWigs_Bosses_Sha of Pride"] = {
			["profiles"] = {
				["2020 Range"] = {
					[-8262] = 515,
				},
				["2020 Default"] = {
					["Swelling Pride"] = 515,
				},
			},
		},
		["BigWigs_Bosses_Kor'kron Dark Shaman"] = {
			["profiles"] = {
				["2020 Range"] = {
					["Iron Tomb"] = 515,
				},
				["2020 Default"] = {
					["Foul Geyser"] = 647,
				},
				["2020 Tank"] = {
					["Foul Geyser"] = 143,
					["Ashen Wall"] = 515,
				},
			},
		},
		["BigWigs_Plugins_Messages"] = {
			["profiles"] = {
				["Default"] = {
					["BWEmphasizeCountdownMessageAnchor_y"] = 371.9110767139318,
					["fontSize"] = 20,
					["BWEmphasizeCountdownMessageAnchor_x"] = 669.1555717587471,
					["BWMessageAnchor_y"] = 590.2221497085338,
					["BWEmphasizeMessageAnchor_y"] = 386.1333426833153,
					["BWMessageAnchor_x"] = 620.0887302928459,
					["font"] = "Friz Quadrata TT",
					["BWEmphasizeMessageAnchor_x"] = 612.266855103444,
				},
				["2020 Heals"] = {
					["BWEmphasizeCountdownMessageAnchor_y"] = 371.9110767139318,
					["fontSize"] = 20,
					["BWEmphasizeCountdownMessageAnchor_x"] = 669.1555717587471,
					["BWMessageAnchor_y"] = 445.8666340602758,
					["BWMessageAnchor_x"] = 617.2443725850826,
					["font"] = "Friz Quadrata TT",
				},
				["2020 Default"] = {
					["BWEmphasizeCountdownMessageAnchor_y"] = 371.9110767139318,
					["fontSize"] = 20,
					["BWEmphasizeCountdownMessageAnchor_x"] = 669.1555717587471,
					["BWMessageAnchor_y"] = 445.8666340602758,
					["BWMessageAnchor_x"] = 617.2443725850826,
					["font"] = "Friz Quadrata TT",
				},
				["2020 Range"] = {
					["BWEmphasizeCountdownMessageAnchor_y"] = 371.9110767139318,
					["fontSize"] = 20,
					["BWEmphasizeCountdownMessageAnchor_x"] = 669.1555717587471,
					["BWMessageAnchor_y"] = 514.844456911087,
					["BWEmphasizeMessageAnchor_x"] = 612.2666814923286,
					["BWMessageAnchor_x"] = 615.822193731201,
					["font"] = "Friz Quadrata TT",
					["BWEmphasizeMessageAnchor_y"] = 446.5778319941637,
				},
				["2020 Melee"] = {
					["BWEmphasizeCountdownMessageAnchor_y"] = 371.9110767139318,
					["fontSize"] = 20,
					["BWEmphasizeCountdownMessageAnchor_x"] = 669.1555717587471,
					["BWMessageAnchor_y"] = 445.8666340602758,
					["BWMessageAnchor_x"] = 617.2443725850826,
					["font"] = "Friz Quadrata TT",
				},
				["2020 Tank"] = {
					["BWEmphasizeCountdownMessageAnchor_y"] = 371.9110767139318,
					["fontSize"] = 20,
					["BWEmphasizeCountdownMessageAnchor_x"] = 669.1555717587471,
					["BWMessageAnchor_y"] = 590.2221497085338,
					["BWEmphasizeMessageAnchor_x"] = 612.266855103444,
					["BWMessageAnchor_x"] = 620.0887302928459,
					["font"] = "Friz Quadrata TT",
					["BWEmphasizeMessageAnchor_y"] = 386.1333426833153,
				},
			},
		},
		["BigWigs_Bosses_Lei Shen"] = {
			["profiles"] = {
				["Feral"] = {
					["Crashing Thunder"] = 643,
				},
			},
		},
		["BigWigs_Plugins_Proximity"] = {
			["profiles"] = {
				["Default"] = {
					["posx"] = 362.6665669414706,
					["fontSize"] = 20,
					["posy"] = 292.9778716776127,
					["font"] = "Friz Quadrata TT",
				},
				["2020 Heals"] = {
					["posx"] = 362.6665669414706,
					["fontSize"] = 20,
					["posy"] = 292.9778716776127,
					["font"] = "Friz Quadrata TT",
				},
				["2020 Default"] = {
					["posx"] = 362.6665669414706,
					["fontSize"] = 20,
					["font"] = "Friz Quadrata TT",
					["posy"] = 292.9778716776127,
				},
				["2020 Range"] = {
					["posx"] = 362.6665669414706,
					["fontSize"] = 20,
					["height"] = 120.0000076293945,
					["font"] = "Friz Quadrata TT",
					["posy"] = 292.9778716776127,
					["width"] = 140.0000305175781,
				},
				["2020 Melee"] = {
					["posx"] = 362.6665669414706,
					["fontSize"] = 20,
					["posy"] = 292.9778716776127,
					["font"] = "Friz Quadrata TT",
				},
				["2020 Tank"] = {
					["posx"] = 362.6665669414706,
					["fontSize"] = 20,
					["posy"] = 292.9778716776127,
					["font"] = "Friz Quadrata TT",
				},
			},
		},
		["BigWigs_Bosses_Twin Consorts"] = {
			["profiles"] = {
				["Default"] = {
					["Nuclear Inferno"] = 643,
				},
			},
		},
		["BigWigs_Plugins_Common Auras"] = {
		},
		["BigWigs_Bosses_Norushen"] = {
			["profiles"] = {
				["2020 Tank"] = {
					[-8218] = 4611,
				},
			},
		},
	},
	["profileKeys"] = {
		["Malvfury - [EN] Evermoon"] = "Default",
		["Malvanis - [EN] Evermoon"] = "2020 Range",
		["Secondferal - [EN] Evermoon"] = "2020 Default",
		["Creampi - [EN] Evermoon"] = "Default",
		["Malvanos - [EN] Evermoon"] = "2020 Tank",
		["Creampuff - [EN] Evermoon"] = "Default",
		["Malvnance - [EN] Evermoon"] = "2020 Default",
	},
	["global"] = {
		["seenmovies"] = {
			[875.1] = true,
			[930.3] = true,
			[953.2000000000001] = true,
		},
	},
	["profiles"] = {
		["Default"] = {
			["showZoneMessages"] = false,
			["blockmovies"] = false,
			["fakeDBMVersion"] = true,
		},
		["2020 Heals"] = {
			["showZoneMessages"] = false,
			["blockmovies"] = false,
			["fakeDBMVersion"] = true,
		},
		["2020 Default"] = {
			["showZoneMessages"] = false,
			["blockmovies"] = false,
			["fakeDBMVersion"] = true,
		},
		["2020 Range"] = {
			["showZoneMessages"] = false,
			["blockmovies"] = false,
			["fakeDBMVersion"] = true,
		},
		["2020 Melee"] = {
			["showZoneMessages"] = false,
			["blockmovies"] = false,
			["fakeDBMVersion"] = true,
		},
		["2020 Tank"] = {
			["showZoneMessages"] = false,
			["blockmovies"] = false,
			["fakeDBMVersion"] = true,
		},
	},
}
BigWigs3IconDB = {
	["minimapPos"] = 190.5003361012038,
}
BigWigsStatisticsDB = {
	[896] = {
		[726] = {
			["lfr"] = {
				["best"] = 251.2909999999974,
				["kills"] = 6,
			},
			["10h"] = {
				["kills"] = 2,
				["wipes"] = 2,
				["best"] = 50.89900000000125,
			},
			["10"] = {
				["kills"] = 6,
				["wipes"] = 3,
				["best"] = 61.51400000000285,
			},
		},
		[679] = {
			["lfr"] = {
				["best"] = 155.3530000000028,
				["kills"] = 2,
			},
			["10h"] = {
				["best"] = 32.39800000000014,
				["kills"] = 1,
			},
			["10"] = {
				["kills"] = 5,
				["wipes"] = 6,
				["best"] = 21.07899999999972,
			},
		},
		[689] = {
			["lfr"] = {
				["best"] = 279.2269999999844,
				["kills"] = 2,
			},
			["10h"] = {
				["best"] = 83.98699999999917,
				["kills"] = 1,
			},
			["10"] = {
				["best"] = 60.0089999999982,
				["kills"] = 5,
			},
		},
		[682] = {
			["lfr"] = {
				["best"] = 140.7019999999902,
				["kills"] = 2,
			},
			["10h"] = {
				["best"] = 51.22100000000137,
				["kills"] = 1,
			},
			["10"] = {
				["best"] = 31.2320000000002,
				["kills"] = 5,
			},
		},
		[677] = {
			["lfr"] = {
				["kills"] = 5,
				["wipes"] = 1,
				["best"] = 264.0339999999997,
			},
			["10h"] = {
				["best"] = 152.2860000000001,
				["kills"] = 1,
			},
			["10"] = {
				["kills"] = 6,
				["best"] = 161.005000000001,
				["wipes"] = 1,
			},
		},
	},
	[886] = {
		[683] = {
			["lfr"] = {
				["best"] = 212.4219999999987,
				["kills"] = 6,
			},
			["10h"] = {
				["wipes"] = 1,
			},
			["10"] = {
				["best"] = 147.9169999999995,
				["kills"] = 8,
			},
		},
		[742] = {
			["lfr"] = {
				["kills"] = 8,
				["wipes"] = 1,
				["best"] = 159.8949999999968,
			},
			["10"] = {
				["kills"] = 9,
				["best"] = 99.44999999999891,
				["wipes"] = 1,
			},
		},
		[729] = {
			["lfr"] = {
				["wipes"] = 10,
			},
			["10"] = {
				["wipes"] = 9,
			},
		},
		[709] = {
			["lfr"] = {
				["kills"] = 8,
				["best"] = 191.1809999999969,
				["wipes"] = 1,
			},
			["10"] = {
				["kills"] = 7,
				["best"] = 44.30299999999988,
				["wipes"] = 1,
			},
		},
	},
	[930] = {
		[818] = {
			["25"] = {
				["best"] = 343.4890000000014,
				["kills"] = 1,
			},
			["25h"] = {
				["kills"] = 3,
				["wipes"] = 2,
				["best"] = 251.0550000000003,
			},
			["10h"] = {
				["kills"] = 6,
				["wipes"] = 19,
				["best"] = 87.23600000000442,
			},
			["lfr"] = {
				["kills"] = 13,
				["wipes"] = 2,
				["best"] = 125.4840000000004,
			},
			["10"] = {
				["kills"] = 5,
				["best"] = 162.9530000000013,
				["wipes"] = 8,
			},
		},
		[820] = {
			["25"] = {
				["best"] = 377.1739999999991,
				["kills"] = 1,
			},
			["25h"] = {
				["kills"] = 3,
				["wipes"] = 1,
				["best"] = 224.4060000000027,
			},
			["10h"] = {
				["kills"] = 5,
				["wipes"] = 11,
				["best"] = 154.5590000000011,
			},
			["lfr"] = {
				["best"] = 144.4489999999998,
				["kills"] = 13,
			},
			["10"] = {
				["best"] = 152.8050000000003,
				["kills"] = 6,
			},
		},
		[824] = {
			["25"] = {
				["kills"] = 1,
				["wipes"] = 1,
				["best"] = 310.5559999999969,
			},
			["25h"] = {
				["kills"] = 3,
				["wipes"] = 3,
				["best"] = 213.7960000000021,
			},
			["10h"] = {
				["kills"] = 4,
				["wipes"] = 104,
				["best"] = 101.8879999999999,
			},
			["lfr"] = {
				["best"] = 5.148000000001048,
				["kills"] = 15,
			},
			["10"] = {
				["kills"] = 10,
				["wipes"] = 20,
				["best"] = 80.57399999999689,
			},
		},
		[828] = {
			["25"] = {
				["kills"] = 1,
				["wipes"] = 1,
				["best"] = 313.1270000000004,
			},
			["25h"] = {
				["best"] = 228.5319999999992,
				["kills"] = 2,
			},
			["10h"] = {
				["kills"] = 7,
				["wipes"] = 7,
				["best"] = 50.40300000000207,
			},
			["lfr"] = {
				["kills"] = 10,
				["best"] = 178.223,
				["wipes"] = 2,
			},
			["10"] = {
				["best"] = 134.2340000000004,
				["kills"] = 3,
			},
		},
		[817] = {
			["25"] = {
				["kills"] = 1,
				["wipes"] = 2,
				["best"] = 713.3559999999998,
			},
			["25h"] = {
				["best"] = 322.1470000000008,
				["kills"] = 3,
			},
			["10h"] = {
				["best"] = 137.6640000000043,
				["kills"] = 5,
			},
			["lfr"] = {
				["kills"] = 21,
				["best"] = 158.4030000000003,
				["wipes"] = 1,
			},
			["10"] = {
				["best"] = 173.8029999999999,
				["kills"] = 7,
			},
		},
		[819] = {
			["25"] = {
				["best"] = 391.0970000000016,
				["kills"] = 1,
			},
			["25h"] = {
				["best"] = 370.7360000000044,
				["kills"] = 2,
			},
			["10h"] = {
				["kills"] = 9,
				["wipes"] = 7,
				["best"] = 132.1790000000001,
			},
			["lfr"] = {
				["kills"] = 13,
				["best"] = 238.6859999999997,
				["wipes"] = 1,
			},
			["10"] = {
				["kills"] = 3,
				["wipes"] = 1,
				["best"] = 350.0469999999987,
			},
		},
		[821] = {
			["25"] = {
				["best"] = 348.2020000000048,
				["kills"] = 1,
			},
			["25h"] = {
				["kills"] = 1,
				["best"] = 308.3620000000001,
				["wipes"] = 1,
			},
			["10h"] = {
				["kills"] = 3,
				["best"] = 208.8389999999999,
				["wipes"] = 18,
			},
			["lfr"] = {
				["kills"] = 15,
				["best"] = 251.985999999999,
				["wipes"] = 1,
			},
			["10"] = {
				["best"] = 238.5,
				["kills"] = 5,
			},
		},
		[816] = {
			["25"] = {
				["best"] = 290.198000000004,
				["kills"] = 1,
			},
			["25h"] = {
				["best"] = 261.857,
				["kills"] = 2,
			},
			["10h"] = {
				["kills"] = 8,
				["wipes"] = 15,
				["best"] = 71.41899999999805,
			},
			["lfr"] = {
				["best"] = 196.7710000000006,
				["kills"] = 13,
			},
			["10"] = {
				["kills"] = 4,
				["wipes"] = 1,
				["best"] = 141.7880000000005,
			},
		},
		[825] = {
			["25"] = {
				["best"] = 221.1829999999973,
				["kills"] = 1,
			},
			["25h"] = {
				["kills"] = 2,
				["best"] = 197.9360000000015,
				["wipes"] = 1,
			},
			["10h"] = {
				["kills"] = 7,
				["wipes"] = 19,
				["best"] = 55.40799999999945,
			},
			["lfr"] = {
				["kills"] = 16,
				["best"] = 132.7730000000011,
				["wipes"] = 5,
			},
			["10"] = {
				["kills"] = 3,
				["wipes"] = 2,
				["best"] = 93.78600000000006,
			},
		},
		[827] = {
			["25"] = {
				["best"] = 166.362000000001,
				["kills"] = 1,
			},
			["25h"] = {
				["kills"] = 3,
				["wipes"] = 2,
				["best"] = 146.1229999999996,
			},
			["10h"] = {
				["kills"] = 9,
				["best"] = 48.78199999999924,
				["wipes"] = 3,
			},
			["lfr"] = {
				["best"] = 171.3300000000018,
				["kills"] = 14,
			},
			["10"] = {
				["kills"] = 3,
				["wipes"] = 3,
				["best"] = 87.76100000000224,
			},
		},
		[829] = {
			["25"] = {
				["best"] = 433.4490000000005,
				["kills"] = 1,
			},
			["25h"] = {
				["best"] = 330.086000000003,
				["kills"] = 3,
			},
			["10h"] = {
				["kills"] = 4,
				["wipes"] = 8,
				["best"] = 161.1699999999983,
			},
			["lfr"] = {
				["kills"] = 22,
				["best"] = 194.9759999999997,
				["wipes"] = 3,
			},
			["10"] = {
				["kills"] = 8,
				["best"] = 163.2839999999997,
				["wipes"] = 5,
			},
		},
		[831] = {
			["10h"] = {
				["wipes"] = 1,
			},
			["25h"] = {
				["wipes"] = 15,
			},
		},
		[832] = {
			["25"] = {
				["kills"] = 1,
				["wipes"] = 1,
				["best"] = 578.4789999999994,
			},
			["25h"] = {
				["kills"] = 3,
				["wipes"] = 8,
				["best"] = 502.5099999999948,
			},
			["10h"] = {
				["best"] = 270.0100000000002,
				["kills"] = 1,
			},
			["lfr"] = {
				["kills"] = 19,
				["best"] = 259.9310000000005,
				["wipes"] = 1,
			},
			["10"] = {
				["kills"] = 10,
				["best"] = 231.851999999999,
				["wipes"] = 1,
			},
		},
	},
	[897] = {
		[744] = {
			["lfr"] = {
				["best"] = 315.916999999994,
				["kills"] = 1,
			},
			["10"] = {
				["best"] = 82.68000000000029,
				["kills"] = 5,
			},
		},
		[737] = {
			["lfr"] = {
				["best"] = 221.8430000000008,
				["kills"] = 4,
			},
			["10"] = {
				["kills"] = 5,
				["best"] = 110.7660000000014,
				["wipes"] = 9,
			},
		},
		[713] = {
			["10"] = {
				["best"] = 77.44999999999982,
				["kills"] = 5,
			},
		},
		[743] = {
			["lfr"] = {
				["wipes"] = 1,
			},
			["10"] = {
				["best"] = 41.59599999999955,
				["kills"] = 4,
			},
		},
		[745] = {
			["lfr"] = {
				["best"] = 269.4259999999995,
				["kills"] = 1,
			},
			["10"] = {
				["best"] = 58.42399999999998,
				["kills"] = 5,
			},
		},
		[741] = {
			["lfr"] = {
				["kills"] = 4,
				["best"] = 199.4880000000012,
				["wipes"] = 2,
			},
			["10"] = {
				["best"] = 61.22599999999966,
				["kills"] = 5,
			},
		},
	},
	[953] = {
		[850] = {
			["25h"] = {
				["best"] = 144.0060000000012,
				["kills"] = 8,
			},
			["10h"] = {
				["kills"] = 6,
				["best"] = 210.6120000000001,
				["wipes"] = 1,
			},
			["flex"] = {
				["kills"] = 16,
				["best"] = 110.4729999999981,
				["wipes"] = 1,
			},
			["lfr"] = {
				["best"] = 294.1569999999992,
				["kills"] = 1,
			},
			["10"] = {
				["best"] = 192.627999999997,
				["kills"] = 9,
			},
		},
		[867] = {
			["25h"] = {
				["best"] = 163.1760000000004,
				["kills"] = 9,
			},
			["10h"] = {
				["kills"] = 9,
				["wipes"] = 2,
				["best"] = 165.3810000000012,
			},
			["lfr"] = {
				["best"] = 307.7780000000003,
				["kills"] = 2,
			},
			["flex"] = {
				["kills"] = 19,
				["best"] = 109.5259999999998,
				["wipes"] = 1,
			},
			["10"] = {
				["best"] = 180.747000000003,
				["kills"] = 11,
			},
		},
		[869] = {
			["25h"] = {
				["kills"] = 9,
				["wipes"] = 20,
				["best"] = 522.1720000000023,
			},
			["flex"] = {
				["kills"] = 23,
				["wipes"] = 15,
				["best"] = 201.969000000001,
			},
			["10h"] = {
				["kills"] = 1,
				["wipes"] = 2,
				["best"] = 503.9480000000003,
			},
			["10"] = {
				["kills"] = 14,
				["wipes"] = 29,
				["best"] = 266.2950000000001,
			},
		},
		[856] = {
			["25h"] = {
				["best"] = 156.8949999999995,
				["kills"] = 9,
			},
			["10h"] = {
				["kills"] = 6,
				["wipes"] = 17,
				["best"] = 216.6050000000005,
			},
			["lfr"] = {
				["best"] = 329.6489999999976,
				["kills"] = 1,
			},
			["flex"] = {
				["kills"] = 20,
				["best"] = 13.61000000000058,
				["wipes"] = 14,
			},
			["10"] = {
				["kills"] = 12,
				["wipes"] = 17,
				["best"] = 118.8059999999969,
			},
		},
		[865] = {
			["normal"] = {
				["wipes"] = 4,
			},
			["25h"] = {
				["kills"] = 8,
				["wipes"] = 37,
				["best"] = 229.726999999999,
			},
			["10h"] = {
				["kills"] = 3,
				["wipes"] = 28,
				["best"] = 190.8139999999994,
			},
			["flex"] = {
				["kills"] = 19,
				["wipes"] = 38,
				["best"] = 92.46299999999974,
			},
			["10"] = {
				["kills"] = 12,
				["wipes"] = 7,
				["best"] = 83.90500000000066,
			},
		},
		[853] = {
			["flex"] = {
				["kills"] = 20,
				["best"] = 14.26699999999983,
				["wipes"] = 13,
			},
			["25h"] = {
				["kills"] = 9,
				["wipes"] = 7,
				["best"] = 504.007999999998,
			},
			["10h"] = {
				["kills"] = 2,
				["wipes"] = 25,
				["best"] = 606.9529999999995,
			},
			["10"] = {
				["kills"] = 12,
				["wipes"] = 5,
				["best"] = 259.0370000000003,
			},
		},
		[849] = {
			["25h"] = {
				["best"] = 203.3020000000001,
				["kills"] = 9,
			},
			["10h"] = {
				["kills"] = 9,
				["best"] = 263.4399999999998,
				["wipes"] = 6,
			},
			["lfr"] = {
				["best"] = 380.2510000000002,
				["kills"] = 2,
			},
			["flex"] = {
				["kills"] = 21,
				["best"] = 170.2139999999999,
				["wipes"] = 6,
			},
			["10"] = {
				["kills"] = 11,
				["best"] = 205.3549999999996,
				["wipes"] = 3,
			},
		},
		[866] = {
			["25h"] = {
				["best"] = 194.5919999999996,
				["kills"] = 9,
			},
			["10h"] = {
				["kills"] = 9,
				["best"] = 224.27,
				["wipes"] = 1,
			},
			["lfr"] = {
				["kills"] = 3,
				["best"] = 346.2240000000002,
				["wipes"] = 1,
			},
			["flex"] = {
				["kills"] = 18,
				["best"] = 39.8060000000005,
				["wipes"] = 18,
			},
			["10"] = {
				["kills"] = 11,
				["best"] = 236.1189999999988,
				["wipes"] = 2,
			},
		},
		[868] = {
			["25h"] = {
				["best"] = 401.3789999999995,
				["kills"] = 9,
			},
			["10h"] = {
				["kills"] = 8,
				["wipes"] = 1,
				["best"] = 416.46,
			},
			["flex"] = {
				["kills"] = 19,
				["wipes"] = 11,
				["best"] = 186.9899999999998,
			},
			["lfr"] = {
				["best"] = 548.5059999999994,
				["kills"] = 1,
			},
			["10"] = {
				["kills"] = 11,
				["wipes"] = 3,
				["best"] = 413.2590000000018,
			},
		},
		[870] = {
			["flex"] = {
				["wipes"] = 36,
			},
			["25h"] = {
				["kills"] = 8,
				["best"] = 306.2570000000014,
				["wipes"] = 5,
			},
			["10h"] = {
				["kills"] = 7,
				["wipes"] = 15,
				["best"] = 361.5310000000009,
			},
			["10"] = {
				["kills"] = 8,
				["best"] = 284.0540000000001,
				["wipes"] = 1,
			},
		},
		[851] = {
			["flex"] = {
				["kills"] = 22,
				["wipes"] = 3,
				["best"] = 38.45800000000054,
			},
			["25h"] = {
				["kills"] = 9,
				["best"] = 203.1290000000008,
				["wipes"] = 5,
			},
			["10h"] = {
				["kills"] = 6,
				["wipes"] = 8,
				["best"] = 198.9810000000016,
			},
			["10"] = {
				["kills"] = 11,
				["best"] = 153.7779999999984,
				["wipes"] = 9,
			},
		},
		[852] = {
			["25h"] = {
				["best"] = 152.3150000000001,
				["kills"] = 9,
			},
			["10h"] = {
				["kills"] = 9,
				["wipes"] = 4,
				["best"] = 238.7180000000001,
			},
			["lfr"] = {
				["best"] = 327.518,
				["kills"] = 2,
			},
			["flex"] = {
				["kills"] = 21,
				["best"] = 24.79399999999987,
				["wipes"] = 2,
			},
			["10"] = {
				["best"] = 215.1059999999998,
				["kills"] = 11,
			},
		},
		[846] = {
			["flex"] = {
				["kills"] = 23,
				["best"] = 138.2060000000001,
				["wipes"] = 22,
			},
			["25h"] = {
				["kills"] = 8,
				["wipes"] = 5,
				["best"] = 170.0939999999991,
			},
			["10h"] = {
				["kills"] = 5,
				["wipes"] = 24,
				["best"] = 199.2989999999991,
			},
			["10"] = {
				["kills"] = 13,
				["best"] = 114.7770000000019,
				["wipes"] = 4,
			},
		},
		[864] = {
			["25h"] = {
				["kills"] = 9,
				["wipes"] = 6,
				["best"] = 160.4149999999991,
			},
			["10h"] = {
				["kills"] = 8,
				["wipes"] = 18,
				["best"] = 142.0819999999999,
			},
			["lfr"] = {
				["kills"] = 1,
				["wipes"] = 1,
				["best"] = 268.5479999999998,
			},
			["flex"] = {
				["kills"] = 18,
				["wipes"] = 3,
				["best"] = 124.8090000000011,
			},
			["10"] = {
				["kills"] = 10,
				["wipes"] = 14,
				["best"] = 226.2570000000014,
			},
		},
	},
}
